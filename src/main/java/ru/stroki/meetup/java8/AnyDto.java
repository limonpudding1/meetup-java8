package ru.stroki.meetup.java8;

import java.util.Objects;

public class AnyDto {

  private Integer number;
  private String text;
  private Boolean active;

  public AnyDto(Integer number, String text, Boolean active) {
    this.number = number;
    this.text = text;
    this.active = active;
  }

  public Integer getNumber() {
    return number;
  }

  public void setNumber(Integer number) {
    this.number = number;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    AnyDto anyDto = (AnyDto) o;
    return Objects.equals(number, anyDto.number)
        && Objects.equals(text, anyDto.text)
        && Objects.equals(active, anyDto.active);
  }

  @Override
  public int hashCode() {
    return Objects.hash(number, text, active);
  }

  @Override
  public String toString() {
    return "AnyDto[" + "number=" + number + ", text='" + text + '\'' + ", active=" + active + ']';
  }
}
